#include "pch.h"
#include "Tile.h"


Tile::Tile(int tilesetNumber, sf::Vector2u tileset_pos, int layer, int tileID) {
	this->tilesetNumber = tilesetNumber;
	this->tileset_pos = tileset_pos;
	this->layer = layer;
	this->tileID = tileID;
}

int Tile::getTileID() {
	return tileID;
}

int Tile::getTilesetNumber() {
	return tilesetNumber;
}


sf::Vector2u Tile::getTilesetPosition() {
	return tileset_pos;
}



Tile::~Tile()
{
}
