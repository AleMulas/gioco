#pragma once
#include "pch.h"
#include "InputKey.h"
#include "MapBuilder.h"
#include <iostream>
#include <vector>
#include <SFML/System.hpp>
#include <SFML/Window.hpp>
#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include <SFML/Network.hpp>
#include <tmxlite/map.hpp>
#include "SFMLOrthogonalLayer.hpp"

int main()
{
	/*
		CONSTANT
	*/
	float MOV_SPEED = 250;


	/*
		GLOBAL VARIABLES
	*/
	float x_offset;
	float y_offset;
	const int NUMBER_OF_LAYER_MAP = 4;

	/*
		SFML THINGS
	*/
	sf::Clock clock;
	sf::RenderWindow window(sf::VideoMode(800, 600), "Test");
	
	//VIEW
	sf::View view;
	view.setSize(1600, 1200);
	view.setCenter(800, 600);
	window.setView(view);
	view.zoom(0.5);
	window.setVerticalSyncEnabled(true);


	/*
		MAP LOADING
	*/
	tmx::Map map;
	if (!map.load("Maps/mappa.tmx")) {
		return 5;
	}
	MapLayer lay_terrain(map, 0);
	MapLayer lay_halfBlocks(map, 1);
	MapLayer lay_buildings(map, 2);
	MapLayer lay_objects(map, 3);
	MapLayer lay_overThePlayer(map, 4);
	MapBuilder mappabyme("Maps/mappa.tmx");
	mappabyme.draw(window);


	/*
		TEXTURE LOADING
	*/
	sf::Texture pg_texture;
	if (!pg_texture.loadFromFile("pg.png"))
	{
		std::cout << "Failed Load From File of the pg texture" << std::endl;
	}
	sf::Sprite pg_sprite;
	pg_sprite.setTexture(pg_texture);
	pg_sprite.setPosition(sf::Vector2f(800, 600));



	/*
		WINDOW LOOP
	*/
	while (window.isOpen())
	{
		sf::Time elapsed = clock.restart();
		float elapsed_time = elapsed.asSeconds();
		float movement;

		/*
			EVENT HANDLING
		*/
		sf::Event event;
		while (window.pollEvent(event)) {
			if (event.type == sf::Event::Closed) {
				window.close();
			}
			if (event.type == sf::Event::KeyPressed) {
				movement = elapsed_time * MOV_SPEED;

			}
		}


		/*
			INPUT HANDLING
		*/
		if (inputKey::handle_movement(&x_offset, &y_offset, movement, &view)) {
			//COLLISION DETECTION

		}
		

		/*
			OPERATION WITH THE WINDOW
		*/
		window.clear(sf::Color::Black);
		window.draw(lay_terrain);
		window.draw(lay_halfBlocks);
		window.draw(lay_buildings);
		window.draw(lay_objects);
		window.draw(lay_overThePlayer);
		window.draw(pg_sprite);
		window.setView(view);
		window.display();
	}
	//window closed, stop the program
	return 0;
}