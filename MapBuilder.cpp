#pragma once
#include "MapBuilder.h"
#include "MapTiles.h"
#include "pch.h"
#include <iostream>
#include <vector>
#include <SFML/System.hpp>
#include <SFML/Window.hpp>
#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include <SFML/Network.hpp>
#include <tmxlite/map.hpp>
#include <tmxlite/TileLayer.hpp>
#include <tmxlite/Tileset.hpp>


MapBuilder::MapBuilder(std::string string_map) {
	tmx::Map map;
	if (map.load(string_map)) {
		std::cout << "Mappa TRUE " << std::endl;
	}
	//LAYERS
	const std::vector<tmx::Layer::Ptr>& allLayers = map.getLayers();
	std::cout << "Size di allLayers: " << allLayers.size() << std::endl;
	//TILE COUNT
	setTileCount(map.getTileCount());
	std::cout << "Tile count: x:" << tile_count.x << " Y:" << tile_count.y << std::endl;
	//TILESET
	tileSets = map.getTilesets();
	std::cout << "Numero di tileset: " << tileSets.size() << std::endl;

	for (int l = 0; l < allLayers.size(); l++) {	// ITERAZIONE DEI LAYER
		if(allLayers.at(l)->getType() == tmx::TileLayer::Type::Tile){
			const tmx::TileLayer tileLayer = *dynamic_cast<const tmx::TileLayer*>(allLayers.at(l).get());
			std::vector<tmx::TileLayer::Tile> tiles = tileLayer.getTiles();
			for (int j = 0; j < tiles.size(); j++) {
				bool trovato = false;
				int i = -1;		  
				tmx::TileLayer::Tile tile = tiles.at(j);
				if (tile.ID == 0) {
					mappa.pushTiles(l, sf::Vector2u(0, 0), -1, tile.ID);
				} else {
					do {
						i++;
						tmx::Tileset tileset = tileSets.at(i);
						if (tile.ID >= tileset.getFirstGID() && tile.ID <= tileset.getLastGID()) {
							trovato = true;
							std::vector<tmx::Tileset::Tile> tilesetTile = tileset.getTiles();
							tmx::Tileset::Tile ts_tile;
							ts_tile = tilesetTile.at(tile.ID - tileset.getFirstGID() - 1);
							mappa.pushTiles(l, sf::Vector2u(ts_tile.imagePosition.x, ts_tile.imagePosition.y), i, tile.ID);
						}
					} while (trovato == false);
				}
			}
		}
		std::cout << "Layer: " << l << " fatto" << std::endl;
	}
}

void MapBuilder::setTileCount(tmx::Vector2u toCopy) {
	tile_count.x = toCopy.x;
	tile_count.y = toCopy.y;
}


tmx::Vector2u MapBuilder::getTileCount() {
	return tile_count;
}


int MapBuilder::draw(sf::RenderTarget& target){
	sf::Vector2u screenSize = target.getSize();
	int num_tiles_x = (15 * screenSize.x) / 1920;
	double dim_tile = screenSize.x / num_tiles_x;

	int j = 0, offset = 0;
	for (unsigned int i = 0; i < mappa.tileCount(); i++) {
		
		Tile* tileToPrint = mappa.getTileAt(i);
		sf::VertexArray square(sf::TriangleStrip, 4);
		square[0].position = sf::Vector2f(i - offset, j);
		square[1].position = sf::Vector2f(20, 0);
		square[2].position = sf::Vector2f(30, 5);
		square[3].position = sf::Vector2f(40, 2);
		
		if ((i + 1) % tile_count.x == 0) {	//Andata a capo
			std::cout << std::endl << std::endl;
			j++;
			offset = j * tile_count.x;
		}
	}



	for (unsigned int i = 0; i < mappa.tileCount(); i++) {
		Tile* tileToPrint = mappa.getTileAt(i);
		std::cout << tileToPrint->getTileID() << ", ";
		if ((i + 1) % tile_count.x == 0) {
			std::cout << std::endl << std::endl;
		}
	}
}



MapBuilder::~MapBuilder(){

}
