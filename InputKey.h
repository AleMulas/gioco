#pragma once
#include <SFML/System.hpp>
#include <SFML/Window.hpp>
#include <SFML/Graphics.hpp>

namespace inputKey {
	enum KeyType {
		enum_UP,
		enum_DOWN,
		enum_LEFT,
		enum_RIGHT,
		enum_ACTION
	};


	////////////////////////////////////////////////////////////
	// set a specific key of the keyboard for input handling
	////////////////////////////////////////////////////////////
	int setKey(KeyType keyToSet, sf::Keyboard::Key new_key);


	////////////////////////////////////////////////////////////
	// verify if a key is pressed in the keyboard
	////////////////////////////////////////////////////////////
	bool isKeyPressed(KeyType keyAsked);

	
	////////////////////////////////////////////////////////////
	// verify if a key is pressed in the keyboard and move the
	// view
	////////////////////////////////////////////////////////////
	bool handle_movement(float *x_offset, float *y_offset, float movement, sf::View *view);
};