#pragma once
#include "Tile.h"
#include <vector>
#include <SFML/System.hpp>

class MapTiles
{
public:
	MapTiles();

	void pushTiles(int layer, sf::Vector2u tilesetPosition, int tilesetNumber, int tileID);

	int tileCount();

	Tile* getTileAt(int i);

	~MapTiles();

private:
	std::vector<Tile*> tiles;
};

