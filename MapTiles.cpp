#include "pch.h"
#include "MapTiles.h"
#include "Tile.h"


MapTiles::MapTiles(){
	
}

void MapTiles::pushTiles(int layer, const sf::Vector2u tilesetPosition, int tilesetNumber, int tileID) {
	// Tile(std::string tileset, sf::Vector2f map_pos, sf::Vector2f tileset_pos, int layer);
	Tile* tile = new Tile(tilesetNumber, tilesetPosition, layer, tileID);
	tiles.push_back(tile);
}

int MapTiles::tileCount() {
	return tiles.size();
}

Tile* MapTiles::getTileAt(int i) {
	return tiles.at(i);
}

MapTiles::~MapTiles()
{
}
