#pragma once
#include <vector>
#include <SFML/System.hpp>
#include <SFML/Window.hpp>
#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include <SFML/Network.hpp>
#include <tmxlite/map.hpp>
#include "MapTiles.h"

class MapBuilder
{
public:
	MapBuilder(std::string string_map);
	int draw(sf::RenderTarget& target);
	tmx::Vector2u getTileCount();
	~MapBuilder();
private:

	std::vector<tmx::Tileset> tileSets;
	MapTiles mappa;
	tmx::Vector2u tile_count;
	void setTileCount(tmx::Vector2u toCopy);
};

