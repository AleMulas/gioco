#pragma once
#include <SFML/System.hpp>

class Tile
{
public:
	Tile(int tilesetNumber, sf::Vector2u tileset_pos, int layer, int tileID);
	
	int getTileID();
	int getTilesetNumber();
	sf::Vector2u getTilesetPosition();
	
	~Tile();

private:
	int tilesetNumber;
	sf::Vector2u tileset_pos;
	int layer;
	int tileID;
};

