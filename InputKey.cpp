#pragma once
#include "pch.h"
#include "InputKey.h"
#include <iostream>
#include <SFML/System.hpp>
#include <SFML/Window.hpp>
#include <SFML/Graphics.hpp>

namespace inputKey {

	namespace {
		sf::Keyboard::Key UP = sf::Keyboard::Key::W;
		sf::Keyboard::Key DOWN = sf::Keyboard::Key::S;
		sf::Keyboard::Key LEFT = sf::Keyboard::Key::A;
		sf::Keyboard::Key RIGHT = sf::Keyboard::Key::D;
		sf::Keyboard::Key ACTION = sf::Keyboard::Key::E;
		sf::Keyboard::Key MENU = sf::Keyboard::Key::Escape;
	}


	int setKey(KeyType keyToSet, sf::Keyboard::Key new_key) {
		switch (keyToSet) {
		case enum_UP:
			UP = new_key;
			break;
		case enum_DOWN:
			DOWN = new_key;
			break;
		case enum_LEFT:
			LEFT = new_key;
			break;
		case enum_RIGHT:
			RIGHT = new_key;
			break;
		case enum_ACTION:
			ACTION = new_key;
			break;
		default:
			std::cout << "Failed to set the key" << std::endl;
			return -1;
			break;
		}
	}


	bool isKeyPressed(KeyType keyAsked) {
		sf::Keyboard::Key keyToVerify;
		switch (keyAsked) {
		case enum_UP:
			keyToVerify = UP;
			break;
		case enum_DOWN:
			keyToVerify = DOWN;
			break;
		case enum_LEFT:
			keyToVerify = LEFT;
			break;
		case enum_RIGHT:
			keyToVerify = RIGHT;
			break;
		case enum_ACTION:
			keyToVerify = ACTION;
			break;
		}
		if (sf::Keyboard::isKeyPressed(keyToVerify)) {
			return true;
		}
		else {
			return false;
		}
	}


	bool handle_movement(float *x_offset, float *y_offset, float movement, sf::View *view) {
		if (isKeyPressed(enum_LEFT)) {
			view->move((movement)*-1, 0);
			*x_offset += (movement * -1);
			std::cout << "Moving left X:" << *x_offset << std::endl;
			return true;
		}
		if (isKeyPressed(enum_RIGHT)) {
			view->move((movement), 0);
			*x_offset += movement;
			std::cout << "Moving right X:" << *x_offset << std::endl;
			return true;
		}
		if (isKeyPressed(enum_UP)) {
			view->move(0, (movement)*-1);
			*y_offset += (movement * -1);
			std::cout << "Moving up Y:" << *y_offset << std::endl;
			return true;
		}
		if (isKeyPressed(enum_DOWN)) {
			view->move(0, (movement));
			*y_offset += movement;
			std::cout << "Moving down Y:" << *y_offset << std::endl;
			return true;
		}
		return false;
	}
}